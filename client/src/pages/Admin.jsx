import React, { useState } from 'react';
import { Button, Container } from 'react-bootstrap';
import CreateBrand from '../components/modals/CreateBrand';
import CreateTypes from '../components/modals/CreateTypes'
import CreateDevice from '../components/modals/CreateDevice';

const Admin = () =>{
    const [brandVisible, setBrandVisible] = useState(false)
    const [typeVisible, setTypeVisible] = useState(false)
    const [deviceVisible, setDeviceVisible] = useState(false)
    return (
 
      <Container className='d-flex flex-column'>
        <Button 
        variant={"outline-dark"} 
        className="mt-2 p-2"
        onClick={()=>setTypeVisible(true)}>
            Добавить Услугу
            </Button>
        <Button 
        variant={"outline-dark"} 
        className="mt-2 p-2"
        onClick={()=>setDeviceVisible(true)}>
            Добавить Мастера
            </Button>
        <Button 
        variant={"outline-dark"} 
        className="mt-2 p-2"
        onClick={()=>setBrandVisible(true)}>
            Добавить Запись
            </Button>
            <CreateBrand show={brandVisible} onHide={() => setBrandVisible(false)}/>
            <CreateDevice show={deviceVisible} onHide={() =>  setDeviceVisible(false)}/>
            <CreateTypes show={typeVisible} onHide={() => setTypeVisible(false)}/>
      </Container>
      

    );
};

export default Admin;