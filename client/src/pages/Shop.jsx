import React from "react";
import { Col, Container, Row } from "react-bootstrap";
import TypeBar from "../components/TypeBar";
import Image from "react-bootstrap/Image";
import backgound from '../assets/fon.png'
const Shop = () =>{
    return(
        <div>
           <Container className="p-3">
                <Row>
                    <Col md={3}>
                    <TypeBar/>
                    </Col>

                    <Col md={8}>
                  
                    <Image width={1000} height={600} src={backgound} 
                    />
                    </Col>
                </Row>
            </Container>
        </div>
    );
};

export default Shop;