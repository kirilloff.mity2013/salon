import React, { useContext } from 'react';
import { Context } from '..';
import {NavLink} from "react-router-dom"
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import { ADMIN_ROUTE, DEVICE_ROUTE, LOGIN_ROUTE, SHOP_ROUTE } from '../utils/consts';
import {Button, Container} from 'react-bootstrap';
import {observer} from "mobx-react-lite";
import { useNavigate } from 'react-router-dom';
import Image from "react-bootstrap/Image";
import logo from "../assets/logo.png";

const NavBar = observer(()=> {
    const navigate = useNavigate()
    const {user} = useContext(Context)
    return (
        <Navbar className='color'>
            <Container>
                <div className="font-roboto " style={{color:"white"}} >Чувство стиля<Image className='size' width={70} height={70} src={logo}/></div>
                
           
            <NavLink  className="font-roboto " style={{color:"white"}} to={SHOP_ROUTE}>Главная</NavLink>
                {user.isAuth ?
            <Nav className="ml-auto" style={{color:'white'}}>
                <Button  className="font-roboto " variant={"outline-light"}
                 onClick={() => navigate(ADMIN_ROUTE)}>
                    Администратор
                 </Button>
                <Button variant={"outline-light"}
                 onClick={() => navigate(LOGIN_ROUTE)} 
                 className="ml-5">
                    Выйти
                    </Button>
            </Nav>
            :
            <Nav className="ml-auto" style={{color:'white'}}>
                <Button variant={"outline-light"} onClick={() => user.setIsAuth(true)}>Авторизация</Button>
          </Nav>    
            }
            </Container>
         
        </Navbar>
    );
});
export default NavBar