import React from "react";
import {Button, Form} from 'react-bootstrap';
import Modal from 'react-bootstrap/Modal';

const CreateDevice = ({show, onHide}) =>{
    return(
      <Modal
        show={show}
        centered
        onHide={onHide}
     
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
          Добавить Мастера
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
        </Modal.Body>
            <Form>
              <Form.Control
                placeholder={"Введите имя мастера"}
                />
            </Form>
        <Modal.Footer>
          <Button variant="outline-danger" onClick={onHide}>Закрыть</Button>
          <Button variant="outline-success" onClick={onHide}>Добавить</Button>
        </Modal.Footer>
      </Modal>
    )
}
export default CreateDevice;