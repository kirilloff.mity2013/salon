import { observer } from "mobx-react-lite";
import React, { useContext } from "react";
import { Card,Form } from "react-bootstrap";
import { Context } from "..";

const BrandBar = observer( () => {

    const {device} = useContext(Context)
    return(
   <Form className="d-flex">
     {device.brands.map(brand => 
     <Card
     style={{cursor:"pointer", padding:"10px", margin:"10px"}}
     key={brand.id}
     className="container px-3"
     onClick={() => device.setSelectedBrand(brand)}
     border={brand.id === device.selectedBrand.id ? "primary":"Light"}
     >
        {brand.name}
     </Card>
     )}
   </Form>
    )
});

export default BrandBar;