import Admin from "./pages/Admin";
import Auth from "./pages/Auth";
import Basket from "./pages/Basket";
import DevicePage from "./pages/DevicePage";
import Shop from "./pages/Shop";
import Destraction from "./pages/Destraction";
import { ADMIN_ROUTE, LOGIN_ROUTE, BASKET_ROUTE, SHOP_ROUTE, REGISTRATIN_ROUTE, DEVICE_ROUTE, DESTRACTION_ROUTE, GLAVNAYA_ROUTE} from "./utils/consts";
import Glavnaya from "./pages/Glavnaya";

export const authRoutes = [
    {
        path: ADMIN_ROUTE,
        Component:Admin
    },
    {
        path: BASKET_ROUTE,
        Component:Basket
    },
]

export const publicRoutes = [
    {
        path: SHOP_ROUTE,
        Component: Shop
    },

    {
        path: LOGIN_ROUTE,
        Component: Auth
    },

    {
        path: REGISTRATIN_ROUTE,
        Component: Auth
    },

    {
        path: DEVICE_ROUTE + '/:id',
        Component: DevicePage
    },
    {
        path: DESTRACTION_ROUTE,
        Component: Destraction
    },
    {
        path: GLAVNAYA_ROUTE,
        Component: Glavnaya
    },
  
]