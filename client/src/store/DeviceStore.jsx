import {makeAutoObservable} from "mobx";

export default class DeviceStore{
    constructor(){
        this._types = [
            {id: 1, name:"О студии"},
            {id: 2, name:"услуги"},
            {id:3, name:"Записаться к мастеру"}
        ]
        this._brands = [
            {id: 1, name:"Коррекция и окрашивание бровей"},
            {id: 2, name:"Маникюр"},
            {id: 3, name:"Макияж"},
            {id: 4, name:"Тонирование волос, Стрижка"},
        ]
        this._devices = [
            {id: 1, name:"Мастер Ирина", price:25000, rating:5, img:''},
            {id: 2, name:"Мастер Ольга", price:25000, rating:5, img:''},
            {id: 3, name:"Мастер Марианна", price:25000, rating:5, img:''},
        ]
        this._selectedBrand = {}
        this._selectedType = {}
        makeAutoObservable(this)
    }
    setTypes(types){
        this._types = types
    }
    setBrands(brands){
        this._brands = brands
    }
    setDevices(devices){
        this._devices = devices
    }
    setSelectedType(type){
        this._selectedType = type
    }
    setSelectedBrand(brand){
        this._selectedBrand = brand
    }
    get types(){
        return this._types
    }
    get brands(){
        return this._brands
    }
    get devices(){
        return this._devices
    }
    get selectedType(){
        return this._selectedType
    }
    get selectedBrand(){
        return this._selectedBrand
    }
}